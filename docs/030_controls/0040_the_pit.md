# The Pit

![the pit](./../images/the_pit.png)

*Notice: The clew (stopper) can not open with a lot of tension on the line. Get the line within for 3 cm and open the clew. Prepare your move and make sure the clew is open before you get the call ‘release’.*

## Left Pit
1.	Tack line:Very important during hoisting spi of jibing. Lots of tension on the line!
2.	Spi halyard: After hoisting stays the line on deck. When not used: inside the cabin
3.	Main halyard: After hoisting the rest of the line in the cabin.
4.	Cunningham:

## Right Pit
1.	Pole up: important to keep the boom up. Make sure the boom is horizontal.
2.	Genua halyard: When the spi is hoisted, lower the genua on command. Preparet he clew is open!
3.	Pole down: When the trimming starts the tack line is released. The pole down takes over. During jibe get within tack line en pole down together, but the tack line takes over the force. Pole down must pulled with the tack line to hold the boom close tot he boat once the tack line is disabled. Otherwise the boom sticks into the water and the boom can snap.
