#Winches, Handles and Grinders

A winch and a grinder are typical extension to increase your body strength. Handle them with care. Handles have the tendency to step overboard, however the do not swim very well.

##Winch
 All winches are turning clockwise. You always have to put the lines clockwise around the winch. A winch gives resistance even when it turning in your direction. Pulling a line in as fast as possible is done faster without a winch. Once the line starts to tension the line should be around the winch. In a lot of situation this change in tension is so abrubt that it is better to have the line around the winch at all times, at least once or twice. The more turns you have around the winch the more tension you can handle. However when you have too many turns around the winch you don't feel the tension and the sheet line its feeling and when releasing the line it does not run out smoothly.

In general three or four turns around the winch is normal for a sheet. Eight to ten turns for the main and head halyards and for the gennaker halyard around five. On the grinder winches is six to seven turns for the genua and two to four for the gennaker. For the gennaker you have to change the number of turns during trimming depending on the puffs and the course of the boat.

 When releasing tension from a sheet on a winch put one hand on the sheet turns on the winch, now you can feel what the sheet does when you **slowly** release the sheet with your other hand.

 ![hand on winch](./../images/hand_on_winch.jpg)

 When you totally want to release a sheet, for example the genua sheet when you are tacking, first release some tension as described before. Once the max tension is off you can take off the sheet from the winch by pulling it up. Please always check if there are no wrinkles in the remaining line, with other words, avoid knots in the line releasing.


## Handle
Now some words about the handles. The handles when used in a winch are locked with a spring, check the spring lock before using. Remove the handle before releasing a sheet or halyard. The handles can be turned clockwise and counter clockwise. In the latter direction the winch goes at half the speed and your power is doubled.


## Grinder
The two big sheet winches on the middle of the boat are attached to one grinder each. The two grinders are equal and are not mirrored. In general it's best that the number one grinder, Bernard, stays on one side of the grinder, that means that on starboard he is on the outside of the grinder and on port on the inside of the grinder. When grinding stay in eye contact with the other grinder and listen to the trimmer who says **trim** and **hold**. Be aware that the boom is close to your head. Especially when it is swinging over during tack or gybe. The grinder has three speeds, clockwise you have **one** and **three** and counter clockwise you have **two**. When sheeting in the genua shift gear when you go from one to two by turning the knob on top of the grinder winch. The above does not sound very clear here but once you are on the water you see what I mean ;) . Check it out while leaving port or before the race.

## One more remark
 One last remark on the grinder winches, this applies to the other winches as well. The lines around the winch should always be in-line with that winch. With other words the line should always be perpendicular to the winch axis and parallel to the deck. In practise this means that the lines should be as close as possible to the deck. All sounds quite complicated but as soon as we are sailing it's more natural and starts to make sense. Time to get sailing.
