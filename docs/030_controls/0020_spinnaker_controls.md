# Spinnaker controls

![spinnaker controls](./../images/spinnaker_controls.png)
Spinnaker controls overview

![spinnaker controls in action with bowman](./../images/spinnaker_controls_photo.png)
Spinnaker controls in action with bowman

![tack line](./../images/tack_line.png)
Tack line

![spinnaker boom_mast_up/down](./../images/boom_horizontal_via_mast.jpg)
Spinnaker boom control mast up/down

![spinnaker boom_toppinglift_up/down](./../images/boom_horizontal_via_topping_lift.jpg)
Spinnaker boom control mast up/down
