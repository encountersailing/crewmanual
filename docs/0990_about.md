This Manual is maintained by the Encounter Sailing Crew

It can be found back online on readthedocs

[http://encountersailing.readthedocs.io](http://encountersailing.readthedocs.io)

The source of the manual is online on gitlab

[https://gitlab.com/encountersailing/crewmanual](https://gitlab.com/encountersailing/crewmanual)

The print version of the manual can be downloaded here

[encountersailingcrewmanual.pdf](./images/encountersailingcrewmanual.pdf)

The current version is :

commit 17dd847e84d0439601a5fc7bf30c01cbc3f40aa6
Author: nosinga <nosinga@gmail.com>
Date:   Wed May 17 10:38:17 2017 +0200

    Update pdf
