# Reefing the main

First we prepare the reeflines. The reeflines go from the gooseneck to the mast plate and from there to a free grinder winch at the middle of the boat. Once the reefline is ready the halyard, sheet and vang can be released. The bowman and the mastman pull the sail downward on the mast till they can attach the reefing cringle to the gooseneck. Once secured the bowman raises his arm and points to the top of the mast. This is the signal to tension the halyard again and to trim in the reefline on the grinder winch. Once both lines are fully tensioned the sheet and vang can be trimmed again. On longer trips it is favourable to secure the clew with an extra holdfast, dutch translation : steekbout.

### keypoints to remember

* prepare reefline on grinder winch
* lower the main halyard
* release vang and sheet
* secure reefing cringle on gooseneck
* tension the halyard
* trim in the reefline
* tension the reefline
* trim the main sheet and vang
* close the stopper in the boom near the gooseneck
* release the reefline
* clear the grinder winch

