# Change Genua to Jib and vv

Changing the genua to jib is by far the most time consuming and thus detrimental manoeuvre and should be avoided at all times. In general reefing the main is the preferred solution however we do describe the genua to jib change. Once again preparation is half the work. We'll first describe the tack change and after that the same change while staying on the same tack. Two more considerations to keep in mind.

1. Due to our handicap rules we only have one forestay groove available during racing. This means that we first have to lower the *old* sail before hoisting the *new* sail.
2. The time spent before the mast by any crew member should be minimised and the crew weight before the mast should be as low as possible. Especially upwind crew weight before the mast is very detrimental for the boat speed.

With these two aspects in mind we come to the following procedure. Have the new sail ready on deck and the bag partially opened, the sheet attached to the clew and the sheet ready to trim. With a tack set the preparation of the sheet is straight forward. Just put in the sheet a windward in the proper blocks. Just be aware of the stays and the guard rail. When staying on the same tack we have to use the bear away sheet which has been described earlier. Back to the prepared *new* sail. The front leech of the sail is just in front of the mast already out of the bag with the sailband securely fastened. Both tack and top are already free. Once everybody is ready the bowman runs forward with the sail by holding the sailband. He attaches the tack to the proper place on the bow and raises his arm. This is the moment the driver steers into the tack and as soon as the *old* sail is inside the guard rail and the stanchions the sail gets dropped at once by releasing the halyard totally. The lowered sail is moved backward as soon as possible. The trick is to get the forleech of the sail next or just before the mast and the rest of the sail behind the mast.

The bowman still at the bow of the boat first opens the tack shackle, during or just after dropping and then removes the halyard from the old top. The halyard gets attached to the *new* sail and the *new* sail is guided through the pre-feeder and the feeder into the groove. As soon as the sail is ready, once again indicated by the raised arm of the bowman the mastman starts to hoist. The driver tries to stay as claused-hauled as possible however the speed is kept at at least four knots. Keep on sailing.

Once the halyard tension is ok on the *new* sail the sheet is trimmed in and the driver is at that time already back the max VMG, velocity made good, course. The *old* sail lies next to the mast and behind. The sail is flaked now. A sailband around the front leach and the sail goes back in the sailbag. When it is extremely wet is stays on deck, otherwise it goes below deck ready to be hoisted again.

Once again, the above described manoeuvre is extremely time consuming and very expensive in terms of lower boat speed for a long time. Normally we hang in to the weather mark and change headsails during the downwind leg.

### keypoints to remember

* get the *new* sail bag on deck, with the front next to the mast
* prepare the *new* sheet
* make bag open
* bowman runs with sail to bow
* bowman attaches the tack to bow
* bowman raises arm
* driver starts to tack
* halyard and sail dropped once inside guard rail and stanchions
* bowman unlocks the tack of the *old* headsail
* bowman transfers halyard from *old* to *new* sail
* bowman feeds sail in pre-feeder and feeder
* bowman raises arm
* mastman hoists
* once in top, tension is put on halyard
* boat already on other tack
* sheet is trimmed
* boat back to target speed
* second sheet secured
* *old* sail flaked
* *old* sail put in bag
* *old* sail put below deck