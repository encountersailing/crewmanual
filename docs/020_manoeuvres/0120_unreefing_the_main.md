# Unreefing the main

Now we go the other way around. If attached remove the holdfast. Now bring tension on the reefline. This can be done with a normal winch and does not have to be done with a grinder winch. Open the stopper inside the boom near the gooseneck. Release the halyard half a meter, free the reeftack from the gooseneck and start hoisting the main. Make sure the cunnuninghamhole is totally free and release vang and mainsheet when putting final tension on the halyard.

### keypoints to remember

* remove holdfast
* tension reefline
* open stopper under boom
* release reefline
* lower halyard half a meter
* free the reeftack near gooseneck
* hoist the main halyard
* release vang and main sheet
* tension the main halyard
* trim the vang and main sheet