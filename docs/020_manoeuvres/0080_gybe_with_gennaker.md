# Gybe with Gennaker

Before gybing the spinnaker pole has to be removed.

Gybing with the gennaker is a lot like gybing with the jib. However there are some notable differences. The tactician indicates that he wants to gybe and starts the manoeuvre by saying **"ready to gybe?"** . First and foremost when there is not a lot of wind the main is brought inside the boat by winching the boom with the main sheet. Now the air from behind can more easily flow into the gennaker and keeps the gennaker filled even when the driver turns the boat from running to dead downwind. The driver keeps on turning till the boat is by the lee. The final call is made by the tactician. **GYBE**. The sheet of the gennaker is release sofar that clew is almost forward of the forestay. At the same time the driver steers even more by the lee and the boom comes over. As soon as the gennaker clew is around the forestay the new gennaker sheet can be trimmed in hard. By turning boat beyond run toward reach the gennaker is moer easile filled with air again. Once the gennaker starts to fill the drive can go down to running course again. And the gennaker sheet has to be eased accordingly.

After gybing the gennaker can be squared again with the spinnaker pole.

### keypoints to remember
* remove spinnaker pole

* **ready to gybe**
* trim in the main
* turn the boat from running to down wind or even by the lee
* bring the gennaker clew forward but keep the gennaker filled
* **GYBE**
* turn the boat more by the lee
* ease the sheet more so the gennaker clew comes forward of the forestay
* swing the boom
* trim the new sheet hard
* turn the boat to reach
* once the gennaker start to fill
* ease the sheet
* turn the boat to running course
* ease the sheet

* square gennaker with spinnaker pole

<!--
## Asymmetric gybe

## When the call is made to jibe
Release the sheet and pull the tack line together with releasing the topping lift till the bowman can reach the end of the boom.
-->