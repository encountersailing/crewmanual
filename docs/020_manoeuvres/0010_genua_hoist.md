# Genua Hoist
The genua when down lies on deck and the forward leach is knotted with a sailband and attached to the boat. The genua lies on one side. Preferably on port side. The advantage is when you hoist the sail you have the right of way. The bowman prepares the halyard, attaches it to the sail and checks if everything is running free. The sail is fed into the pre-feeder and the feeder. Then the bowman relieves the genua shock cord and unties the front of the sail by removing the sailband.

When the bowman is ready and the sail is ready to hoist the bowman points with his strecht arm to the top of the mast.
The mastman and halyards man can start hoisting now. Hoisting the sail should go as quick as possible. The driver steers the boat upwind on such an angle that the sheet corner, the clew, is about one meter outside of the boat.

The bowman stays at the front till the sail is almost up and he only helps on the halyard for the last half meter to hoist the sail.

Extensive slack on the sheet should be avoided. However no tension should be on the sheet during the hoist of the sail. When the sail is totally up and has the proper halyard tension, the sail can be powered up by either grinding in the sheet or by bearing away a bit.

### keypoints to remember
* genua on proper board
* genua halyard attached
* genua top in feeder
* genua free of shockcord and sailband
* bowman indicates ready for hoist
* **HOIST**
* no tension on sheet
* tension on halyard
* tension on sheet