# Spinnaker Pole down

Removing the pole is the opposite of setting the pole. First **dé-**square the pole. The pole is brought to the front. The pole should not brought totally to the forestay. Because by easing the toppinglift and lowering the pole the pole also moves forward towards the forestay. When easing the guy and the toppinglift one should keep proper tension on the downhaul. Also the tack-line should be trimmed quite severe. When all goes well the pole end should end within the pulpit. During this movement most of the time the gennaker sheet has to be trimmed in at the same time, to keep the gennaker properly trimmed.

Now the guy can be released and all the tension gets to the tackline. By giving enough slack on the guy the guy can be removed from the pole beak and the pole can be stored away. The final steps is to bring the toppinglift back to the mast.

### keypoints to remember
* bring pole forward by easing the guy
* lower the pole end by easing the toppinglift
* keep winching the downhaul
* start trimming the tackline
* ease the guy when the pole end is within the pulpit
* release and slakc the guy
* lower the pole end
* remove the guy from the pole beak
* lower the pole on the mast
* store the pole
* bring the toppinglift back to the mast

