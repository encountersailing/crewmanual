# Start
Around eleven minutes before the actual start we make a final call which sail will be hoisted, the final call is made by Bart and Sven. Around 10 to 7 minutes before the start that sail will be hoisted and the other sails are stored under deck. An hour or so before the race we make some practise runs and train on the different manoeuvres and check the actual speed, the condition on the course and so on. In the last last 10 minutes before the starting signal a final plan for the start is made and communicated. Decision is made on where to start on the starting line. Boat or pin-end etc. The manoeuvring before the start is also important. To go from one board to the other gybing is preferred for two reasons. One you keep the boat up to speed. Two the upwind distance to the starting line is kept larger so the boat and crew can keep a higher speed. It is best when the boat is positioned in such a place that you don’t have to make a gybe or tack in the last one-and-a-half minute. In the final approach to the starting line the bowman indicates the distance to the line by holding up fingers to indicate the boat length to the line. Crossing the line should be done at full upwind speed or even more speed by steering a little bit off wind. Apart from all the individuals tasks the general crew has to position their weight in such a way that the boat goes as fast as possible. And of course crossing the line should be done after the starting signal.

### keypoints to remember
* decision which head sail the One or the Three
* **10 minutes signal**
* hoist headsail
* make and communicate starting plan
* **5 minutes signal**
* execute plan
* one-and-a-half minute, final approach
* **one minute signal**
* trim prepared to get more than max boatspeed
* choose final position on line
* **START**
