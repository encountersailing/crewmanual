# Weather mark rounding

After the start and a good deal of the upwind beat we get towards the weather mark. The tactician communicates his strategy and explains the options with regard to the other boats around. The tactician also makes the final call on which gennaker will be used. And finally he indicates if he wants a bear away set or a gybe set. Or a bear away set immediately followed by a gybe with the gennaker.<br>
Now the bowman can prepare the gennaker sheets and bring the shackles to the proper stanchion. The bowman excutes this task in close coordination with the other crewmembers. Except the bowman all the oher crewmembers remain in there position, **don't move**.<br>
Now it is time to bring the gennaker on deck. The bowman brings the sail to front halfway between the mast and the bow and attaches the gennaker bag to the boat near a stanchion. After securing the bag the sheets are attached to the clew and the tackline to the tack. Finally it is time for gennaker halyard. The halyard is prepared and looked after by the mastman. He hands the halyard shackle to the bowman. The bowman attaches the halyard shackle to the head of the gennaker.<br>
At the same time the pit prepares the genua halyard for dropping the genua, ease and remove from winch the cunningham and the temporary bear away genua sheet is installed. This bear away sheet is used to free the big genua winch. This big winch can now be used for the gennaker sheet. The gennaker sheets are prepared for trimming, however there should not never be tension on any gennaker sheet till the mastman shouts **top**. All should be ready about two boat lengths before the mark. Within the last two boat length all the crewmembers get to their position. The gennaker is ready to hoist.<br>
When we reach the buoy and the boat starts to bear away both the main and the genua are eased. During the turn or sometimes up to ten boat lengths after the rounding depending on tactical considerations the tactician shouts **ready to hoist** and **HOIST**. Sometimes only **HOIST**. The mastman hoists the gennaker as fast as he can. It is very important that the sail does not catch wind during the hoist. When the gennaker is in the top of the mast the mastman shouts **TOP**. Now the sheet from which the slack already is removed is trimmed in fast in order to fill the genakker. At the same time the halyard of the genua is released and the bowman pulls the genua down and together with one or two other crewmembers brings the genua to deck inside the guard rail and over the stanchions. After tying the genua with a sailband and shockcord and putting the genua in the feeder again the halyard of the genua is tensioned again. In this way the genua is ready to hoist again.

Depending on the course and speed the spinnaker pole is set. This manoeuvre is the subject of the following chapter.

After setting the pole the mainsail halyard can be released a bit as well.

We are now well on our way towards the next mark. However before we get there we are going to set and remove the spinnaker pole and do some gybes as well. These subjects are described in the next chapters. Before we go over to the next chapter we repeat the keypoints to remember.

### keypoints to remember
* which gennaker ?
* bear away or gybe set ?
* prepare gennaker sheets
* bring gennaker on deck
* secure gennaker bag on deck
* attach sheets and tackline to gennaker
* attach halyard to gennaker
* bear away sheet installed
* prepare genua halyard
* remove cunningham from winch
* enter the two lengths to buoy
* everybody in position
* bear away
* ease main and genua
* **HOIST**
* hoist gennaker
* secure tack
* **TOP**
* trim the sheet hard
* lower the genua
* trim the gennaker properly
<br>
<br>
* set the pole (next chapter)
<br>
<br>
* prepare the genua ready to hoist
* remove gennaker bag
* release tension on mainsail halyard



<!--
# Asymmetric hoist

We set up our asymmetric spinnaker for inside jibes where the clew passes between the spinnaker and the headstay during the jibe, and we launch out of the forward hatch.

## Setting the spinnaker
As we come to the weather mark, the crew is hiking, and if there's an offset in place, we stay on the rail until the boat flattens on the reach to the offset.

The key to having good sets is marking everything
As we start to round the offset:

* Pull the tack line to the mark:
*	As the mastman makes the call: hoist the spinnaker: Hoist!
*	As soon as the spinnaker is clear of the hatch, allows the spinnaker to fill more quickly.

## When the call is made to jibe
Release the sheet and pull the tack line together with releasing the topping lift till the bowman can reach the end of the boom.

###Coordinating the drop
The bowman always stays forward. When the call is made to drop the left pitman releases the xxx Everyone – without task - helps to get the kite down. One moves below to become the sewer man during the douses.

It's important to control the halyard drop so that the kite is brought around to the weather side. The forward bowman concentrates on gathering the foot, making sure the entire length of it is on board, and the aft bowman gathers the belly (running a leech tape as he does so). The sewer grabs the belly and pulls it through the hatch (also running a leech tape to make sure it's coming in clean).
-->
