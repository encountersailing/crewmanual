# Gennaker Gybe Peel
The gennaker gybe peel is used to change to a flatter or fuller gennaker. Sometimes it is executed during a gybe mark rounding. For example going from a run to a reach. But most of the times it is a specific gybe to execute this manoeuvre.

Once again preparation is half of the game. First attach the sailbag halfway the mast and the bow next to the guard rail and near a stanchion. Then attach a spare sheet to the *new* gennaker and attach the tack to a temporary tack line. This is all executed on the windward board. Prepare the halyard. Once everything is ready the driver steers a deep downwind run and we hoist the new gennaker. The mastman shouts top once the *new* gennaker is in top. **Gybe** and we lower the *old* genakker, get that gennaker below deck, change the sheets, pack the *old* gennaker in its bag. You can watch the following [gybe peel video. https://www.youtube.com/watch?v=gf_dalLAuoE](https://www.youtube.com/watch?v=gf_dalLAuoE)

### keypoints to remember
* get gennaker bag on deck
* secure bag on deck
* attach spare sheet to *new* gennaker
* attach temporary tack-line to *new* gennaker
* attach halyard to *new* gennaker
* steer on a deep run
* hoist the *new* gennaker
* **TOP**
* **GYBE**
* lower the *old* gennaker
* get gennaker below deck
* change back from spare sheet to regular gennaker sheets
* pack the *old* gennaker into the bag