# Gybe with Genua or Jib
There are up to three situations where we gybe without a downwind sail up during the race.

1. During the pre-start period. A gybe is preferred above a tack when manoeuvring. This is for the following two reasons. Reason one is to keep the boat moving at considerable speed so you can go where you want. Reason two is avoiding the tendency to get to close to the starting line. With a big boat speed at the starting signal is more important than the exact position.
2. A gybe set at the windward buoy. In this situation a gybe is executed before the gennaker is hoisted. Executing this manoeuvre can be dictated by the tacktics or by the race course, not all courses are up-down wind.
3. A gybe just before or during the leeward buoy rounding. Sometimes it is preferable to have the downwind sail stowed away before gybing near the leeward buoy.

The tactician dictates where to go and especially in the pre-start period it should be clear to the crew when a gybe is about due. The tactician can say gybing but this is not necessary. The driver turns the boat from running to dead down wind and the main sheet trimmer trims in the main. The angle off the boom should always be less than 45 degrees compared with the centerline of the boat. When the boom is reasonably far in the new backstay can be prepared and tensioned. Now the driver can say Gybe. Be aware when the boom comes over. The boom always claims right of way. Stay out of that way. Now the old backstay has to be released to give room to the boom. It sounds like a contradiction but here the headsail trimmer should do the same. When turning from running to dead down wind the headsail should be trimmed in, to the point where the clew is between the stanchions and above deck. The new trimmer has removed the extensive slack from the new sheet and at the point that the boat is dead down wind the grinders go to the new grinder on the new winch around which the new sheet is wrapped. When the sheet is trimmed in properly at the old side. There is no reason to trim-in more. The opposite actually when being back at running course the sheet has to be released till the sail is trimmed well. When the boat keeps on turning to half wind or upwind. Of course keep on trimming the head and main accordingly.

### keypoints to remember
* **gybing**
* trim the main sheet in to less than 45 degrees of centerline
* turn the boat from running to dead down wind
* keep trimming in the main
* trim the clew of headsail on to the deck
* tension the backstay
* **GYBE**
* gybe the boom, by steering the boat towards the new running course
* gybe the headsail
* ease the old backstay
* ease the main
* ease the headsail