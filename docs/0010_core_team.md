The core team consists of one women and eight men. I’ll introduce you to each of them. Whenever something is not clear grab one of us and ask your question.

## Helmsman : Bart
First and foremost we have Bart. Bart is the owner skipper and driver. He is at the helm so to speak. Without Bart there is no boat and without a boat there is no race. Let me tell you that Bart fulfills a lot more duties than only keeping the boat in more than excellent shape.

## Mast & Pit : Ed
Second there is Ed. Ed is also owner of the boat. Ed knows of all the technical details of the boat. When you break something, lose something or can not find something. You go to Ed. Ed you can find around the mast and most of the time he is on the halyards. When he is not there, well then he is somewhere else.

## Tactician : Sven
Third there is Sven. Sven is the brains of the boat and is the liaison between the owners and the other core crew members. Sven is a childhood friend of Bart and both guys grew up on a  houseboat on a dutch river called “De Vecht” as neighbours. You can find Sven at the back of the boat. Sven stands behind Bart and makes the calls. If Sven says right we go to the right. Unless Bart steers to the left. Then the boat goes to the left. That’s the nature of things.

## Bowman : Paul
Now I like to introduce you to Paul. Paul is the bowman and most of the time he is the first to finish. You can find him on the bow of the boat. Only he is allowed before the mast at all times. Most of our manoeuvring is dictated by what is happening before the mast. Since we are all looking forward and Paul is in front most of the time we are looking at Paul.

## Halyards : Mascha
Finally. Snow white. We have more girls on board but only one in the core team. Macha is in charge of hoisting and lowering the sails. During the race we normally only hoist and lower sails in front of the mast, but even that keeps us quite busy.

## Sail and Crew master : Nanne
Nanne is keeping an eye on the boat and one on the crew. Nanne does a lot of talking and currently he is talking to you. Hi there that’s me Nanne. Normally you find me after the mast and before the grinders.<br>
My nick name is **speed doctor**. My speciality is increasing speed of information transformation. This can be translated to increasing speed of learning, increasing speed of communication, increasing speed of coördination, increasing speed of trust and last but not least increasing boat speed.


Now I introduce you to the guys who run the engine and turn the throttle or better said who are on the sheets.

## Head Sail and Gennaker Trimmer : Hugo
Hugo you find on the sheets of all the sails you see in front of the mast. During the race we go upwind and downwind and have different sails for that purpose. Ed, Mascha and Paul are taking care of bringing them up and down. But even more important is Hugo. Hugo his task is to get most speed adding power out of the sails by trimming the sheets. Hugo is most of the time near the big sheet winches.

## Grinder : Bernard
Hugo holds the sheet, Bernard trims the sheet. Bernard ate more spinach in his live than the rest of the crew together. The boat is strong, Bernard is stronger. Aptly named Popeye, he is the real sailor man. Bernard came on board recently but he is a real key core crew member. Apart from Sven and Bart, Bernard will never miss a race. And Bernard, being a local, knows where to go when the boat is in the harbor.

## Main Sail Trimmer : Hans
Last but not least there is Hans. Hans is on the main. Actually Hans is steering the boat. The boat has a big rudder. The main is bigger. If Sven says bear away and Bart turns the wheel. Well the boat will stay straight on track if Hans does not ease the sheet. Once again that is the nature of things. Hans has his own cock-pit.

Are you still with us ? Good ! Now you are thinking what’s in it for me. Let me tell you. We sail the boat with twelve to fourteen crew members so there are three to five spots left and you will find your spot there. Moreover except for Bart, Sven and Bernard who are on the boat at all times the other core crew positions are open as well occasionally.
