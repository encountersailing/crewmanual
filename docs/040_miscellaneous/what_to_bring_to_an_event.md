|NR	|Item|	Opmerking  |
|:---:|----|-------------|
|1.	|Paspoort en boekingsbevestigingen	|
|2.	|Bijdragen in de pot: max 60 euro pd|	Afhankelijk van wat Panerai organisatie biedt als borrels en diner.
|3.	|Zeiljack zonder voering met los fleecevest|
|4.	|Zeilbroek (?)	|Check de weersvoorspellingen voor vertrek
|5.	|Boot- en walschoenen|	Minimaal 2 paar. Encounter betreden uitsluitend met |de bootschoenen!
|6.	|Slippers	|Voor op het land en douchen
|7.	|Kniebeschermers voor het dekwerk|	Optioneel. Ligt aan je functie
|8.	|(Zeil-) korte broek en t-shirts|	Van de Panerai krijg je een t-shirt en een petje. Tijdens de wedstrijden draag je een Encounter shirt.
|9. |	Zwempak en handdoek
|10.|	Oplader telefoon
|11.|	Zeilhandschoenen|	Optioneel. Ligt aan je functie
|12.|	Slaapzak/lakenzak/sloop|	Slaap je in het appartement dan is dit meestal niet nodig.
|13.|	Starthorloge	|Optioneel
|14.|	Zonnebril met touwje|
|15.|	Toilettas	|
|16.|	Zonnebrand en na ‘t zonnetje|
|17.| ...	| ...
