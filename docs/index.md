# Welcome on board
Hi there, welcome on board. Currently you are onboard of the Encounter, a well maintained 1975 German Frers designed sailing boat. Encounter is an 53 feet aluminium sloop built by Palmer & Johnson. More details about the boat you can find [here, http://www.encountersailingyacht.com](http://www.encountersailingyacht.com/). Just for now. Bart and Ed found the boat as an abandoned wreck on a ship graveyard in Florida. They totally restored the boat in France. The Encounter is a sistership of the Australian Bumblebee III, which was owned by John Kahlbetze.
The list of honours for the Bumblebee III.

             2nd: Sydney/Hobart Race - 1974
             Australian Admiral’s Cup team - 1975
             1st: Miami/Nassau Race - 1976
             1st: Ft Lauderdale/Charlston Race - 1976
             1st: Newport Race Week - 1976
             2nd: Newport/Bermuda Race - 1976
             1st: Skaw Race - 1977
             1st: Skaw Race Week - 1977
The Encounter has quite some potential in the classic race circuit in the Mediterrane.

In this manual you find how we race the Encounter on the Med and probably you are here to find out how you can contribute to go faster and win even more races with us.

![encounter antibes 2015](./images/encounter_antibes.jpg)
[encounter antibes 2015]

We like you to walk through the structure of this manual.
This manual continues with the introduction of the core crew who maintain this manual, followed by the description of a typical Med Race from start to finish.

* The First part, this introduction followed by an introduction to the core team
* The Second describes the Main Manoeuvres, it starts with a typical Med Race and some general safety precautions.
* The Third part describes More Manoeuvres which are related to change the sail area when the wind conditions change from moderate to heavy or vice versa.
* The Fourth part is the Miscellaneous part which starts with the positions on Deck, a detailed description of some of the controls and technics on board. It also contains the leftovers like what to bring on board.

This manual is based on two main sources of knowledge.
First and foremost the condensed knowledge of the nine crewmembers we'll introduce to you in a second.
Second the book by Stuart Quarrie “[The offshore crew’s manual](https://www.amazon.com/Offshore-Race-Crews-Manual/dp/1408157284/ref=sr_1_1?ie=UTF8&qid=1481373180&sr=8-1&keywords=The+offshore+crew%E2%80%99s+manual)". This book is somewhere in the book cabinet on board.

This manual primarerely concentrates on boat handling during manoeuvres while racing. This document only describes manoeuvring, trimming and tackticks are not described in this manual.

When you want to know everything you need to know about trim, please watch the  excellent North Sails video called [“Trim for speed”, https://www.youtube.com/watch?v=P1n-7czmc14](https://www.youtube.com/watch?v=P1n-7czmc14). It is mandatory for racing on the Encounter to watch this video **at least** once a year.

When you want to know more about tactics and strategy. Talk to the guys at the after guard. But only talk to them after the race.

In summary. If you want to find out more. Grab one of the before mentioned mighty nine or even better watch the 58 minutes of [Trim for speed](https://www.youtube.com/watch?v=P1n-7czmc14) or of course read the entire book by Stuart Quarrie.

Of course we race the boat together and are all equal but some are more equal than others. Before we go into depth on manoeuvering and other technical details I like to introduce you to the core crew. Knowing who is who is most of the times more important than knowing what is what. The core crew can be described as the eight dwarfs and snow white.
